FROM ruby:2-alpine AS build

COPY . /app
WORKDIR /app

RUN set -eux; \
    apk add --no-cache --virtual .build-deps \
        gcc \
        g++ \
        make \
        musl-dev; \
    bundle install; \
    bundle exec jekyll build -d build/

FROM nginx:1.17-alpine AS app

COPY --from=build /app/build /usr/share/nginx/html
